CFLAGS = -Wall -ggdb

TARGET = powerTunes

SOURCES = $(TARGET).c libmpdclient/libmpdclient.c

all: $(TARGET)

$(TARGET): $(SOURCES)

clean:
	rm -f *.o $(TARGET)
