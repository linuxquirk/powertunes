/* powerTune.c */
/* MPD control interface using powerMate */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/soundcard.h>
#include <linux/input.h>

#include "libmpdclient/libmpdclient.h"

/*
	program configuration
*/


/* CHANGE HERE TO FIT YOUR CONFIGURATION */
#define MPD_TARGET_HOST "localhost"
#define MPD_TARGET_PORT 6678
#define POWER_MATE   "/dev/input/event0"

#define TRACK_MOVE_SENSITIVITY 10
#define PULSE_PLAYBACK_SPEED 252
#define PULSE_PAUSE_SPEED 258
#define PULSE_PROCESSING_COMMAND_SPEED 264
#define LED_BRIGHTNESS 0x80

/*
	debug flag
*/

//#define POWERTUNE_DEBUG_MODE 1

#define  BUFFER_SIZE 32

/*
	structure used within this program
*/

typedef struct pMateLEDCtrl {
	int static_brightness;
	int pulse_speed;
	int pulse_table;
	int pulse_asleep;
	int pulse_awake;
} pMateLEDCtrl;

/*
	status & command flags
*/

#define MPD_STATUS_TOTAL MPD_STATUS_STATE_PAUSE

#define POWERTUNE_IDLE	MPD_STATUS_STATE_STOP
#define POWERTUNE_PLAY	MPD_STATUS_STATE_PLAY
#define POWERTUNE_PAUSE	MPD_STATUS_STATE_PAUSE
#define POWERTUNE_NEXT	MPD_STATUS_TOTAL + 1
#define POWERTUNE_PREV	MPD_STATUS_TOTAL + 2
#define POWERTUNE_RANDOM	MPD_STATUS_TOTAL + 3
#define POWERTUNE_WAIT	MPD_STATUS_TOTAL + 4

/*
	utility macros
*/

#if defined(POWERTUNE_DEBUG_MODE)
#define _debug_printf(msg...) \
	printf("DBG: ");  \
	printf(msg)
#else
#define _debug_printf(msg...)
#endif

/*
	prototypes
*/

int open_pMate(char *deviceName);
int get_mpd_playback_status(mpd_Connection * conn);
void powermate_pulse_led(int fd, pMateLEDCtrl *LEDCtrl);

/*
	here comes main()...
*/

/*****************************************************/
/* main() */
/*****************************************************/
int main(int argc, char ** argv)
{
	mpd_Connection * conn;
	int powerMate = -1;
	
	int playbackStat = -1, lastPlayBackStat = -1;
	int commandStat = -1;
	int isButtonPressed = 0;
	int ButtonPressCounter = 0;
	
	int trackSelect = 0;
	
	int statusUpdate = 0;
	int commandUpdate = 0;
	
	int r = 0;
	struct input_event ibuffer[BUFFER_SIZE];
	
	pMateLEDCtrl LEDCtrl;
	
	printf("powerTunes Ver. 0.10\n");
	
	/************************************/
	/* initialize LED control variables */
	/************************************/
	LEDCtrl.static_brightness = LED_BRIGHTNESS;
	LEDCtrl.pulse_speed = 255;
	LEDCtrl.pulse_table = 1;
	LEDCtrl.pulse_asleep = 1;
	LEDCtrl.pulse_awake = 0;
	
	/******************/
	/* connect to MPD */
	/******************/
	conn = mpd_newConnection(MPD_TARGET_HOST,MPD_TARGET_PORT,10);

	if(conn->error) {
		fprintf(stderr,"%s\n",conn->errorStr);
		mpd_closeConnection(conn);
		return -1;
	}
	
	printf("MPD Version: ");
	printf("%i.%i.%i\n", conn->version[0],conn->version[1], conn->version[2]);
	
	/************************************/
	/* Now, try connecting to powerMate */
	/************************************/
	if((powerMate = open_pMate(POWER_MATE)) < 0){
		// error!!
		printf("powermate not found at: %s\n", POWER_MATE);
		goto end;
	}
	
	/********************************/
	/* OK, here comes the main loop */
	/********************************/
	while(1){
		
		/* initialize status flag */
		statusUpdate = 0;
		commandUpdate = 0;
		trackSelect = 0;
		
		/*******************************/
		/* Get current status from MPD */
		/*******************************/
		playbackStat = get_mpd_playback_status(conn);
		
		if(playbackStat < 0){
			// error in get_mpd_playback_status()
			printf("error while acquiring MPD status!!\n");
			goto end;
		}
		
		if(playbackStat != lastPlayBackStat){
			/* raise status update flag */
			statusUpdate = 1;
		}
		
		/* set last MPD playback status */
		lastPlayBackStat = playbackStat;
		
		/***********************************/
		/* Get current status of powerMate */
		/***********************************/
		r = read( powerMate , ibuffer, sizeof(struct input_event) * BUFFER_SIZE);
		if(r > 0){
			/* Some event have occured */
			int events = 0, i = 0;
			events = r / sizeof(struct input_event);
			//_debug_printf("some event coming up from powerMate!! %d\n", events);
			/* for each event... */
			for(i=0; i < events; i++){
				switch(ibuffer[i].type){
				case EV_REL:
					 _debug_printf("Button was rotated %d units\n", (int)ibuffer[i].value);
					/* find out which direction of rotation somehow... */
					trackSelect += (int)ibuffer[i].value;
					if(trackSelect > TRACK_MOVE_SENSITIVITY){
						// move on to next track
						_debug_printf("command: jump to next track\n");
						commandUpdate = 1;
						commandStat = POWERTUNE_NEXT;
					}
					if(trackSelect < -TRACK_MOVE_SENSITIVITY){
						// move on to next track
						_debug_printf("command: jump to previous track\n");
						commandUpdate = 1;
						commandStat = POWERTUNE_PREV;
					}
					break;
				case EV_KEY:
					_debug_printf("Button was %s\n", ibuffer[i].value? "pressed":"released");
					if(ibuffer[i].value == 1){
						// pressed
						isButtonPressed = 1;
					}else{
						// released
						isButtonPressed = 0;
						if(ButtonPressCounter >= 2){
							// long click!!
							_debug_printf("long click detected!!\n");
							// ummm, what should i do??
							// mpd_sendShuffleCommand(conn) ??
							commandStat = POWERTUNE_IDLE;
						}else{
							// short click
							_debug_printf("short click detected!!\n");
							if(playbackStat == POWERTUNE_PLAY){
								commandStat = POWERTUNE_PAUSE;
							}else{
								commandStat = POWERTUNE_PLAY;
							}
						}
						commandUpdate = 1;
						
						/* initalize press counter */
						ButtonPressCounter = 0;
					}
					break;
				case EV_MSC:
					//_debug_printf("The LED pulse settings were changed; code=0x%04x, value=0x%08x\n", ibuffer[i].code, ibuffer[i].value);
					break;
				default:
					// unexpected input...
					fprintf(stderr, "Warning: unexpected event type; ibuffer[i].type = 0x%04x\n", ibuffer[i].type);
					break;
				}
			}
		}
		
		/**************************************************/
		/* Now let's think about how our powerMate behave */
		/**************************************************/
		if(statusUpdate == 1){
			switch(playbackStat){
			case POWERTUNE_PAUSE:
				// pause
				_debug_printf("playback is paused\n");
				LEDCtrl.pulse_speed = PULSE_PAUSE_SPEED;
				LEDCtrl.pulse_awake = 1;
				break;
			case POWERTUNE_PLAY:
				// play
				_debug_printf("playback is ongoing\n");
				LEDCtrl.pulse_speed = PULSE_PLAYBACK_SPEED;
				LEDCtrl.pulse_awake = 1;
				break;
			case POWERTUNE_IDLE:
				// stop
				_debug_printf("playback is at rest\n");
				LEDCtrl.pulse_awake = 0;
				break;
			default:
				/* we are not supposed to reach here!! */
				_debug_printf("invalid playback status is set: %d\n", playbackStat);
				break;
			}
		}
		
		/*************************************/
		/* set latest playback status to MPD */
		/*************************************/
		if(commandUpdate == 1){
			
			// make LED to brink fast to notify that
			// the command is being processed.
			_debug_printf("processing command...\n");
			LEDCtrl.pulse_speed = PULSE_PROCESSING_COMMAND_SPEED;
			LEDCtrl.pulse_awake = 1;
			
			mpd_sendCommandListOkBegin(conn);
			
			switch(commandStat){
			case POWERTUNE_IDLE:
				// stop playback
				mpd_sendStopCommand(conn);
				break;
			case POWERTUNE_PLAY:
				if(lastPlayBackStat == POWERTUNE_IDLE){
#if 0 /* resume play should be implemented here... */
					// stop -> play
					if(song->pos!=MPD_SONG_NO_NUM){
						_debug_printf("i dunno which track i'm on now...\n");
						mpd_sendPlayCommand(conn, MPD_PLAY_AT_BEGINNING);
					}else{
						mpd_sendPlayCommand(conn, conn->pos);
					}
#endif
					// just for now...
					mpd_sendPlayCommand(conn, MPD_PLAY_AT_BEGINNING);
				}else{
					// pause -> play
					mpd_sendPauseCommand(conn, 0);
				}
				break;
			case POWERTUNE_PAUSE:
				// play -> pause
				mpd_sendPauseCommand(conn, 1);
				break;
			case POWERTUNE_PREV:
				// move to previous track
				mpd_sendPrevCommand(conn);
				break;
			case POWERTUNE_NEXT:
				// move to next track
				mpd_sendNextCommand(conn);
				break;
			}
			
			// enable cross fade
			mpd_sendCrossfadeCommand(conn, 8);
			
			mpd_sendCommandListEnd(conn);
			mpd_finishCommand(conn);
			
			// set last_status = wait
			// so that when it gets next status,
			// statusUpdate flag is turned on
			lastPlayBackStat = POWERTUNE_WAIT;
		}
		
		/**********************************/
		/* set latest status to powerMate */
		/**********************************/
		if((statusUpdate == 1) || (commandUpdate == 1)){
			powermate_pulse_led(powerMate, &LEDCtrl);
		}
		
		/* deal with long press */
		if(isButtonPressed == 1) ButtonPressCounter++;
		
		/* zzz... */
		sleep(1);
		}
	
end:
	
	/***********************/
	/* disconnect from MPD */
	/***********************/
	mpd_closeConnection(conn);
	
	return 0;
}

/*
	utility functions
*/

/*********************************************/
/* open_pMate() */
/*********************************************/
int open_pMate(char *deviceName){
	
	int ret = -1;
	
	if (( ret = open(deviceName, O_RDWR|O_NONBLOCK)) < 0)
     {
	    perror( POWER_MATE );
	    return ret;
     }
	return ret;
}

/*********************************************/
/* get_mpd_playback_status() */
/* get status from MPD and return status->state */
/*********************************************/
int get_mpd_playback_status(mpd_Connection * conn){
	
	mpd_Status * status;
	int ret = 0;
	
	mpd_sendCommandListOkBegin(conn);
	mpd_sendStatusCommand(conn);
	mpd_sendCommandListEnd(conn);
	
	if((status = mpd_getStatus(conn))==NULL) {
		printf("ERROR: mpd_getStatus()\n");
		fprintf(stderr,"%s\n",conn->errorStr);
		return -1;
	}
	
	ret = status->state;
	
	mpd_freeStatus(status);
	
	mpd_finishCommand(conn);
	
	return ret;
}

/*********************************************/
/* powermate_pulse_led() */
/*********************************************/
void powermate_pulse_led(int fd, pMateLEDCtrl *LEDCtrl)
{
  struct input_event ev;
  memset(&ev, 0, sizeof(struct input_event));
  
  LEDCtrl->static_brightness &= 0xFF;

  if(LEDCtrl->pulse_speed < 0)
    LEDCtrl->pulse_speed = 0;
  if(LEDCtrl->pulse_speed > 510)
    LEDCtrl->pulse_speed = 510;
  if(LEDCtrl->pulse_table < 0)
    LEDCtrl->pulse_table = 0;
  if(LEDCtrl->pulse_table > 2)
    LEDCtrl->pulse_table = 2;
  LEDCtrl->pulse_asleep = !!LEDCtrl->pulse_asleep;
  LEDCtrl->pulse_awake = !!LEDCtrl->pulse_awake;

  ev.type = EV_MSC;
  ev.code = MSC_PULSELED;
  ev.value = LEDCtrl->static_brightness | (LEDCtrl->pulse_speed << 8) | (LEDCtrl->pulse_table << 17) | (LEDCtrl->pulse_asleep << 19) | (LEDCtrl->pulse_awake << 20);

  if(write(fd, &ev, sizeof(struct input_event)) != sizeof(struct input_event))
    fprintf(stderr, "write(): %s\n", strerror(errno));  
}

/* EOF */
